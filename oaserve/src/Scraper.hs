{-# LANGUAGE OverloadedStrings #-}

module Scraper
    ( scrapeAchievements
    , startScraper
    ) where

import Control.Concurrent
    ( forkIO
    , MVar
    , putMVar
    , takeMVar
    , threadDelay
    , ThreadId
    )
import Control.Monad (forever)
import Data.List (uncons, length, null)
import Data.Maybe (catMaybes)
import Data.Text (pack)
import Data.Text.Encoding (decodeUtf8)
import Data.Text.Read (hexadecimal)
import Network.HTTP.Simple (httpLBS, parseRequest, getResponseBody)
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.ByteString.Lazy.Internal as BSLI
import qualified Database.Persist.Postgresql as P
import Text.Regex.TDFA ((=~))

import Env
    ( Env
    , decrementScrapeCount
    , getPool
    , getScrapeCount
    , setScrapeCount
    , stopScraper
    , waitScraper
    )
import Models
    ( Achievement (..)
    , defaultAchievement
    , dequeueTag
    , getAchievementsCount
    , getQueueLength
    , setAchievements
    , setTagFailed
    , setTagScraped
    , setUnlocks
    , Tag
    , tagURL
    , Unlock (..)
    , unTagKey
    , updatePercents
    )
import Websockets (broadcast, State)

-- URLS

sneakySandyURL :: String
sneakySandyURL =
    "https://playoverwatch.com/en-us/career/pc/us/SneakySandy-1439"

playOverWatchURL :: String
playOverWatchURL =
    "https://playoverwatch.com/en-us/career/"

-- Regex

achievementRegex :: BSLI.ByteString
achievementRegex =
    "<div[^<>]*><div[^<>]*><img src=\"([^\"]+)\" [^<>]*><div[^<>]*><div[^<>]*>[^<>]+</div></div></div><div id=\"achievement-0x([0-9A-F]+)\"[^<>]*><span[^<>]*></span><h6[^<>]*>([^<>]+)</h6><p[^<>]*>([^<>]+)</p></div></div>"

unlockRegex :: BSLI.ByteString
unlockRegex =
    "<div data\\-tooltip=\"achievement\\-0x([0-9A-F]+)\" class=\"tooltip media\\-card achievement\\-card( m\\-disabled)?\">"

-- Period to wait between scrapes, in microseconds.
scrapePeriod :: Int
scrapePeriod = 5000000 -- 5s

-- Start the scraper
startScraper :: Env -> MVar State -> IO ThreadId
startScraper env wsState =
    forkIO (forever (do
        waitScraper env
        scrapedCount <- getScrapeCount env
        if scrapedCount > 0 then do
            scrapeOne env wsState
            decrementScrapeCount env
            threadDelay scrapePeriod
        else do
            queueLength <- getQueueLength (getPool env)
            if queueLength == 0 then do
                Prelude.putStrLn "Scraping done"
                stopScraper env
            else
                setScrapeCount env queueLength))

-- Scrape one tag from the queue.
scrapeOne :: Env -> MVar State -> IO ()
scrapeOne env wsState = do
    maybeTag <- dequeueTag (getPool env)
    case maybeTag of
        Just P.Entity {P.entityKey = tagId, P.entityVal = tag} -> do
            let url = playOverWatchURL ++ tagURL tag
            Prelude.putStrLn ("Scraping " ++ (show url))
            request <- parseRequest url
            response <- httpLBS request
            let body = getResponseBody response
            let matches = (body =~ unlockRegex)
            let unlocks = catMaybes (map (matchToUnlock tagId) matches)
            let scrapedCount = length unlocks
            dbCount <- getAchievementsCount (getPool env)
            if scrapedCount == dbCount then do
                setUnlocks (getPool env) unlocks
                setTagScraped (getPool env) tagId
                updatePercents (getPool env)
                broadcast
                    wsState
                    (pack (show (P.unSqlBackendKey (unTagKey tagId))))
            else do
                setTagFailed (getPool env) tagId
        Nothing ->
            Prelude.putStrLn "Scrape queue is empty"

-- Make an unlock with a tag and the data from a regex match.
matchToUnlock :: P.Key Tag -> [BSLI.ByteString] -> Maybe Unlock
matchToUnlock tagId (html:matches) =
    case uncons matches of
        Just (id, [disabled]) ->
            case hexadecimal (decodeUtf8 (BSL.toStrict id)) of
                Left _ ->
                    Nothing
                Right (poid, _) ->
                    Just (Unlock { unlockTagId = tagId
                                 , unlockAchievementId = P.toSqlKey poid
                                 , unlockUnlocked = BSL.null disabled
                                 })
        Nothing ->
            Nothing

-- ö

-- Scrape the achievements and add them into the database.
scrapeAchievements :: P.ConnectionPool -> IO ()
scrapeAchievements pool = do
    Prelude.putStrLn "Scraping achievement information"
    request <- parseRequest sneakySandyURL
    response <- httpLBS request
    let body = getResponseBody response
    let matches = (body =~ achievementRegex)
    let achievements = catMaybes (map matchToAchievement matches)
    if null achievements then
        Prelude.putStrLn "Failed to scraped achievements"
    else do
        let scrapedCount = length achievements
        Prelude.putStrLn ("Scraped " ++ (show scrapedCount) ++ " achievements")
        dbCount <- getAchievementsCount pool
        Prelude.putStrLn ("Already have " ++ (show dbCount) ++ " achievements")
        if scrapedCount == dbCount then
            Prelude.putStrLn ("Don't do anything")
        else
            setAchievements pool achievements

-- Make an achievement with the data from a regex match.
matchToAchievement :: [BSLI.ByteString] -> Maybe Achievement
matchToAchievement [html, image, id, name, description] =
    case hexadecimal (decodeUtf8 (BSL.toStrict id)) of
        Left _ ->
            Nothing
        Right (poid, _) ->
            Just (defaultAchievement { achievementPoid = poid
                                     , achievementName = BSL8.unpack name
                                     , achievementDescription =
                                        BSL8.unpack description
                                     , achievementImage = BSL8.unpack image
                                     })
