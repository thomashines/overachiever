{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Models where

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.Trans.Reader (ReaderT)
import Data.List (uncons)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.Time (UTCTime, getCurrentTime)
import GHC.Int (Int64)
import Network.HTTP.Types.URI (urlDecode, urlEncode)
import qualified Data.Aeson.TH as TH
import qualified Data.ByteString.Char8 as BS8
import qualified Data.ByteString.Lazy as BSL
import qualified Database.Persist.Postgresql as P
import qualified Database.Persist.TH as TH

-- Models

TH.share
    [TH.mkPersist TH.sqlSettings, TH.mkMigrate "migrateAll"]
    [TH.persistLowerCase|
Achievement
    poid Int
    category Int
    name String
    description String
    image String
    percent Int
    UniqueAchievement poid
    deriving Show

Tag
    server String
    battletag String
    code String
    scraped UTCTime Maybe
    failed UTCTime Maybe
    deriving Show

Unlock
    tagId TagId
    achievementId AchievementId
    unlocked Bool
    UniqueUnlock tagId achievementId
    deriving Show

Queue
    tagId TagId
    priority Int
    queued UTCTime
    UniqueQueue tagId
    deriving Show

PlayerList
    url String
    page Int
    playercount Int
    scraped UTCTime Maybe
    deriving Show
|]

TH.deriveJSON TH.defaultOptions ''Achievement
TH.deriveJSON TH.defaultOptions ''Queue
TH.deriveJSON TH.defaultOptions ''Tag
TH.deriveJSON TH.defaultOptions ''Unlock
TH.deriveJSON TH.defaultOptions ''PlayerList

-- Queries

insertAchievementSQL :: Data.Text.Text
insertAchievementSQL =
    " insert into achievement                                           \
    \ (id, poid, category, name, description, image, percent)           \
    \ values (?, ?, ?, ?, ?, ?, ?) returning ??;                        "

getTagUnlocksAchievementsSQL :: Data.Text.Text
getTagUnlocksAchievementsSQL =
    " select ??, ??                                                     \
    \ from unlock, achievement                                          \
    \ where unlock.tag_id=? and achievement.id=unlock.achievement_id;   "

updatePercentsSQL :: Data.Text.Text
updatePercentsSQL =
    " with scrapedcount as (select count(*) scrapedcount                \
    \                       from tag                                    \
    \                       where scraped is not null)                  \
    \ update achievement                                                \
    \ set percent = (1000 * count) / scrapedcount                       \
    \ from ((select achievement_id, count(unlocked or null) as count    \
    \        from unlock                                                \
    \        group by achievement_id) as counts                         \
    \       cross join scrapedcount) allcounts                          \
    \ where allcounts.achievement_id = id;                              \
    \ select count(*) from tag where scraped is not null;               "

getLastPlayerListSQL :: Data.Text.Text
getLastPlayerListSQL =
    " select ??                                                         \
    \ from player_list                                                  \
    \ where playercount > 0                                             \
    \ and url like ?                                                    \
    \ order by page desc                                                \
    \ limit 1;                                                          "

-- Model methods

-- Blank Achievement record.
defaultAchievement :: Achievement
defaultAchievement = Achievement
    { achievementPoid = 0
    , achievementCategory = 0
    , achievementName = ""
    , achievementDescription = ""
    , achievementImage = ""
    , achievementPercent = 0
    }

-- Blank Tag record.
defaultTag :: Tag
defaultTag = Tag
    { tagServer = ""
    , tagBattletag = ""
    , tagCode = ""
    , tagScraped = Nothing
    , tagFailed = Nothing
    }

-- Blank PlayerList record.
defaultPlayerList :: PlayerList
defaultPlayerList = PlayerList
    { playerListUrl = ""
    , playerListPage = 0
    , playerListPlayercount = 0
    , playerListScraped = Nothing
    }

-- Build a tag from a list of properties.
buildTag :: [BS8.ByteString] -> Maybe Tag
buildTag [server, battletag, code] =
    Just (defaultTag { tagServer = BS8.unpack (urlDecode False server)
                     , tagBattletag = BS8.unpack (urlDecode False battletag)
                     , tagCode = BS8.unpack (urlDecode False code)
                     })
buildTag [server, battletag] =
    Just (defaultTag { tagServer = BS8.unpack (urlDecode False server)
                     , tagBattletag = BS8.unpack (urlDecode False battletag)
                     })
buildTag _ =
    Nothing

-- Get nice battletag from tag
tagNiceBattletag :: Tag -> String
tagNiceBattletag tag =
    case tagCode tag of
        [] ->
            tagBattletag tag
        _ ->
            (tagBattletag tag) ++ "#" ++ (tagCode tag)

tagURL :: Tag -> String
tagURL tag =
    let ulrBattletag = BS8.unpack (urlEncode False (BS8.pack (tagBattletag tag)))
        ulrCode = BS8.unpack (urlEncode False (BS8.pack (tagCode tag)))
    in case tagServer tag of
        "pc-eu" ->
            "pc/eu/" ++ ulrBattletag ++ "-" ++ ulrCode
        "pc-kr" ->
            "pc/kr/" ++ ulrBattletag ++ "-" ++ ulrCode
        "pc-us" ->
            "pc/us/" ++ ulrBattletag ++ "-" ++ ulrCode
        "psn" ->
            "psn/" ++ ulrBattletag
        "xbl" ->
            "xbl/" ++ ulrBattletag
        _ ->
            (tagServer tag) ++ "/" ++ ulrBattletag ++ "-" ++ (tagCode tag)

-- Utility methods

-- Convert Lazy.ByteString into Data.Text.
lazyByteStringToText :: BSL.ByteString -> Data.Text.Text
lazyByteStringToText =
    decodeUtf8 . BSL.toStrict

stringToText :: String -> Data.Text.Text
stringToText = decodeUtf8 . BS8.pack

-- Database methods

-- Get the full details from a partially complete tag or insert it into the DB.
getOrInsertTag :: P.ConnectionPool -> Tag -> IO (Maybe (P.Entity Tag))
getOrInsertTag pool tag =
    P.runSqlPersistMPool (do
        -- Look up tag in database.
        tags <- P.selectList [ TagServer P.==. (tagServer tag)
                             , TagBattletag P.==. (tagBattletag tag)
                             , TagCode P.==. (tagCode tag)
                             ]
                             [P.LimitTo 1]
        case uncons tags of
            Just (tagEntity, _) ->
                return (Just tagEntity)
            Nothing -> do
                -- Insert the tag.
                tagId <- P.insert tag
                -- Get the inserted tag.
                maybeTagValue <- P.get tagId
                -- Return the id, tag, or nothing.
                case maybeTagValue of
                    Just tagValue ->
                        return (Just (P.Entity { P.entityKey = tagId
                                               , P.entityVal = tagValue
                                               }))
                    Nothing ->
                        return (Nothing)
    ) pool

-- Get a list of the unlocks and related achievements for a tag from the database.
getTagUnlocksAchievements :: P.ConnectionPool -> P.Key Tag
    -> IO [(P.Entity Unlock, P.Entity Achievement)]
getTagUnlocksAchievements pool tagKey =
    P.runSqlPersistMPool (getTagUnlocksAchievementsWithSQL tagKey) pool

-- Insert an achievement into the database with id=poid and return the entity.
getTagUnlocksAchievementsWithSQL :: MonadIO m => P.Key Tag
    -> ReaderT P.SqlBackend m [(P.Entity Unlock, P.Entity Achievement)]
getTagUnlocksAchievementsWithSQL tagKey =
    P.rawSql getTagUnlocksAchievementsSQL [P.toPersistValue tagKey]

-- Add a tag to the queue table.
queueTag :: P.ConnectionPool -> P.Key Tag -> Int -> IO ()
queueTag pool tagId priority =
    P.runSqlPersistMPool (do
        -- Try to get the queue record for this tag.
        queue <- P.getBy (UniqueQueue tagId)
        case queue of
            Just queueEntity ->
                -- Update the priority.
                P.update
                    (P.entityKey queueEntity)
                    [QueuePriority P.=. priority]
            Nothing -> do
                -- Add the tag to the queue
                now <- liftIO getCurrentTime
                void (P.insert (Queue tagId priority now))
    ) pool

-- Pop a tag from the queue.
dequeueTag :: P.ConnectionPool -> IO (Maybe (P.Entity Tag))
dequeueTag pool =
    P.runSqlPersistMPool (do
        queues <- P.selectList
                    []
                    [P.Asc QueuePriority, P.Asc QueueQueued, P.LimitTo 1]
        case uncons queues of
            Just (P.Entity {P.entityKey = queueKey, P.entityVal = queue}, _)
                -> do
                    -- Got queue entry, delete it.
                    P.delete queueKey
                    -- Get the associated tag key.
                    let tagKey = queueTagId queue
                    -- Get the associated tag.
                    maybeTag <- P.get tagKey
                    case maybeTag of
                        Just tag ->
                            return (Just (P.Entity { P.entityKey = tagKey
                                                   , P.entityVal = tag
                                                   }))
                        Nothing ->
                            return Nothing
            Nothing ->
                return Nothing) pool

-- Get the number of achievements in the database.
getAchievementsCount :: P.ConnectionPool -> IO Int
getAchievementsCount pool =
    P.runSqlPersistMPool (P.count ([] :: [P.Filter Achievement])) pool

-- Get the list of achievements in the database.
getAchievements :: P.ConnectionPool -> IO [P.Entity Achievement]
getAchievements pool =
    P.runSqlPersistMPool (P.selectList ([] :: [P.Filter Achievement]) []) pool

-- Update a list of achievements in the database.
setAchievements :: P.ConnectionPool -> [Achievement] -> IO ()
setAchievements pool achievements =
    flip P.runSqlPersistMPool pool $
        sequence_ $
            map (\achievement -> do
                maybeExisting <- P.getBy (UniqueAchievement
                                    (achievementPoid achievement))
                case maybeExisting of
                    Just entity ->
                        P.update
                            (P.entityKey entity)
                            [ AchievementCategory P.=.
                                (achievementCategory achievement)
                            , AchievementName P.=.
                                (achievementName achievement)
                            , AchievementDescription P.=.
                                (achievementDescription achievement)
                            , AchievementImage P.=.
                                (achievementImage achievement)
                            , AchievementPercent P.=.
                                (achievementPercent achievement)
                            ]
                    Nothing -> do
                        void (insertAchievement achievement))
                achievements

-- Insert an achievement into the database with id=poid and return the entity.
insertAchievement :: MonadIO m => Achievement
    -> ReaderT P.SqlBackend m [P.Entity Achievement]
insertAchievement achievement =
    P.rawSql
        insertAchievementSQL
        [ P.toPersistValue (achievementPoid achievement)
        , P.toPersistValue (achievementPoid achievement)
        , P.toPersistValue (achievementCategory achievement)
        , P.toPersistValue (achievementName achievement)
        , P.toPersistValue (achievementDescription achievement)
        , P.toPersistValue (achievementImage achievement)
        , P.toPersistValue (achievementPercent achievement)
        ]

-- Update a list of unlocks in the database.
setUnlocks :: P.ConnectionPool -> [Unlock] -> IO ()
setUnlocks pool unlocks =
    flip P.runSqlPersistMPool pool $
        sequence_ $
            map (\unlock -> do
                maybeExisting <- P.getBy (UniqueUnlock
                                    (unlockTagId unlock)
                                    (unlockAchievementId unlock))
                case maybeExisting of
                    Just entity ->
                        P.update
                            (P.entityKey entity)
                            [UnlockUnlocked P.=. (unlockUnlocked unlock)]
                    Nothing -> do
                        void (P.insert unlock))
                unlocks

-- Set a tag's scraped time to now.
setTagScraped :: P.ConnectionPool -> P.Key Tag -> IO ()
setTagScraped pool tagKey =
    flip P.runSqlPersistMPool pool $ do
        now <- liftIO getCurrentTime
        P.update tagKey [TagScraped P.=. Just now]

-- Set a tag's failed time to now.
setTagFailed :: P.ConnectionPool -> P.Key Tag -> IO ()
setTagFailed pool tagKey =
    flip P.runSqlPersistMPool pool $ do
        now <- liftIO getCurrentTime
        P.update tagKey [TagFailed P.=. Just now]

-- Get the number of scraped tags in the database.
getTagCount :: P.ConnectionPool -> IO Int
getTagCount pool =
    P.runSqlPersistMPool (P.count [TagScraped P.!=. Nothing]) pool

-- Update the achievement percent unlocked in the database.
updatePercents :: P.ConnectionPool -> IO [P.Single Int]
updatePercents pool =
    P.runSqlPersistMPool (P.rawSql updatePercentsSQL []) pool

-- Get the length of the queue from the database.
getQueueLength :: P.ConnectionPool -> IO Int
getQueueLength pool =
    P.runSqlPersistMPool (P.count ([] :: [P.Filter Queue])) pool

-- Get the last PlayerList from the database for the given url
getLastPlayerList :: P.ConnectionPool -> String -> IO (Maybe PlayerList)
getLastPlayerList pool url = do
    playerLists <- (P.runSqlPersistMPool
                    (P.rawSql
                        getLastPlayerListSQL
                        [P.toPersistValue (url ++ "%")])
                    pool) :: IO [P.Entity PlayerList]
    case uncons playerLists of
        Just (playerList, []) -> return (Just (P.entityVal playerList))
        Nothing -> return Nothing

-- Insert a new PlayerList
insertPlayerList :: P.ConnectionPool -> PlayerList -> IO ()
insertPlayerList pool playerList =
    P.runSqlPersistMPool
        (do
            -- Add the tag to the queue
            now <- liftIO getCurrentTime
            void (P.insert (playerList { playerListScraped = Just now })))
        pool

-- Get a tag by its id
getTagById :: P.ConnectionPool -> Integer -> IO (Maybe (P.Entity Tag))
getTagById pool tagId =
    P.runSqlPersistMPool
        (do
            let tagKey = P.toSqlKey (fromIntegral tagId)
            maybeTag <- P.get tagKey
            case maybeTag of
                Just tag ->
                    return (Just (P.Entity { P.entityKey = tagKey
                                           , P.entityVal = tag
                                           }))
                Nothing ->
                    return Nothing)
        pool
