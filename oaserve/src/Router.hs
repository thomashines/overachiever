module Router
    ( Route (..)
    , routeFinder
    ) where

import Data.List (uncons)
import Network.Wai (Application)
import qualified Data.ByteString.Char8 as BS8
import Text.Regex.TDFA ((=~))

import Env (Env)

-- Router

-- Router entries.
data Route backend = Route
    { path :: BS8.ByteString
    , method :: BS8.ByteString
    , handler :: [BS8.ByteString] -> Env -> Application
    }

-- Return the first matching route in a list of routes.
routeFinder :: BS8.ByteString -> BS8.ByteString -> [Route backend]
    -> Maybe (Route backend, [BS8.ByteString])
routeFinder matchPath matchMethod routes =
    -- Get the next route to check.
    case uncons routes of
        Just (route, remainingRoutes) ->
            if matchMethod == method route then
                let vars = (matchPath =~ (path route))
                in if null vars then
                    -- Route path didn't match.
                    routeFinder matchPath matchMethod remainingRoutes
                else
                    -- Route matched.
                    Just (route, (head vars))
            else
                -- Route method didn't match.
                routeFinder matchPath matchMethod remainingRoutes
        Nothing ->
            -- No routes matched.
            Nothing
