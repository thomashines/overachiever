{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Lib (oaserve) where

import Control.Concurrent (newMVar)
import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.Logger
    ( LoggingT
    , runLoggingT
    , LogSource
    , LogLevel
    , LogStr
    , Loc
    )
import Data.Aeson (decode)
import Data.String (fromString)
import Network.HTTP.Types (Status)
import Network.Wai
    ( Application
    , rawPathInfo
    , Request
    , requestMethod
    )
import Network.Wai.Handler.Warp
    ( defaultSettings
    , runSettings
    , setHost
    , setLogger
    , setPort
    )
import Network.Wai.Handler.WebSockets (websocketsOr)
import Network.Wai.Middleware.Cors (simpleCors)
import Network.WebSockets (defaultConnectionOptions)
import qualified Data.Aeson.TH as TH
import qualified Data.ByteString.Char8 as BS8
import qualified Data.ByteString.Lazy as BSL
import qualified Database.Persist.Postgresql as P
import System.IO (Handle, stdout)

import Env (Env, makeEnv, incrementScrapeCount)
import Handlers
    ( getAchievementsHandler
    , getTagCountHandler
    , getTagHandler
    , indexHandler
    , notFoundHandler
    , playerHandler
    , queueTagHandler
    , staticFileHandler
    )
import Models (migrateAll)
import PlayerListScraper (startPlayerListScraper)
import Router (Route (..), routeFinder)
import Scraper (scrapeAchievements, startScraper)
import Websockets (State, websocketApp)

-- Config

data Config = Config
    { host :: String
    , port :: Int
    , dbhost :: String
    , dbname :: String
    , dbpass :: String
    , dbport :: Int
    , dbuser :: String
    } deriving (Show)

TH.deriveJSON TH.defaultOptions ''Config

-- Initialisation

-- Make a postgresql connection string from a config.
makeConnectionString :: Config -> BS8.ByteString
makeConnectionString config =
    BS8.pack (Prelude.concat [ "host=", (dbhost config)
                             , " dbname=", (dbname config)
                             , " user=", (dbuser config)
                             , " password=", (dbpass config)
                             , " port=", (show (dbport config))
                             ])

-- Logger function called for every request.
logger :: Request -> Status -> Maybe Integer -> IO ()
logger request status code =
    Prelude.putStrLn ((show (requestMethod request)) ++ ": " ++
        (show (rawPathInfo request)))

-- Run persist logging.
runPersistLoggingT :: MonadIO m => LoggingT m a -> m a
runPersistLoggingT =
    (`runLoggingT` persistOutput stdout)

-- Persist logging formatter.
persistOutput :: Handle -> Loc -> LogSource -> LogLevel -> LogStr -> IO ()
persistOutput h loc src level msg =
    return ()

-- Main method: run WARP application.
oaserve :: IO ()
oaserve = do
    -- Try to load config file.
    confJSON <- BSL.readFile "conf.json"
    -- Try to decode config.
    let config = decode confJSON
    case config of
        Nothing ->
            Prelude.putStrLn "failed to parse conf.json"
        Just c ->
            let connStr = makeConnectionString c
            -- Initialise the database connection pool.
            in runPersistLoggingT $ P.withPostgresqlPool connStr 4 $
                \pool -> liftIO $ do
                    -- Migrate database.
                    P.runSqlPersistMPool (P.runMigration migrateAll) pool
                    -- Scrape achivements.
                    scrapeAchievements pool
                    -- Make websockets state MVar
                    wsState <- Control.Concurrent.newMVar []
                    -- Make the environment record.
                    env <- makeEnv pool
                    -- Run scraper thread.
                    thread <- startScraper env wsState
                    -- Start the scraper
                    incrementScrapeCount env
                    -- Start the player list scraper
                    _ <- startPlayerListScraper env
                    -- Serve application.
                    liftIO $ Prelude.putStrLn ("serving at http://" ++
                        (host c) ++ ":" ++ (show $ port c))
                    let settings =
                            setHost (fromString (host c)) $
                            setLogger logger $
                            setPort (port c) $
                            defaultSettings
                    runSettings settings $
                        websocketsOr
                            defaultConnectionOptions
                            (websocketApp wsState) $
                        simpleCors $
                        app env

-- Router

-- WARP application.
app :: Env -> Application
app env request =
    -- Get the handler based on the request path and method.
    case routeFinder (rawPathInfo request) (requestMethod request) routes of
        Just (route, vars) ->
            -- Call the handler.
            (handler route) vars env request
        Nothing ->
            -- Return a 404.
            notFoundHandler request

-- Routes

-- The list of routes and handlers.
routes :: [Route backend]
routes =
    [ Route "^/?$" "GET" indexHandler
    , Route "^/static/(.+)$" "GET" staticFileHandler
    , Route "^/(pc-eu|pc-kr|pc-us)/([^/\\-]+)-([0-9]+)/?$" "GET" playerHandler
    , Route "^/(pc-eu|pc-kr|pc-us)/([^/\\-]+)-([0-9]+)/?$" "POST" playerHandler
    , Route "^/(psn|xbl)/([^/]+)/?$" "GET" playerHandler
    , Route "^/(psn|xbl)/([^/]+)/?$" "POST" playerHandler
    , Route "^/api/(pc-eu|pc-kr|pc-us)/([^/\\-]+)-([0-9]+)/?$" "GET" getTagHandler
    , Route "^/api/(pc-eu|pc-kr|pc-us)/([^/\\-]+)-([0-9]+)/?$" "POST" queueTagHandler
    , Route "^/api/(psn|xbl)/([^/]+)/?$" "GET" getTagHandler
    , Route "^/api/(psn|xbl)/([^/]+)/?$" "POST" queueTagHandler
    , Route "^/api/global/?$" "GET" getAchievementsHandler
    , Route "^/api/global/count/?$" "GET" getTagCountHandler
    ]
