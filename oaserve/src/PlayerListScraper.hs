{-# LANGUAGE OverloadedStrings #-}

module PlayerListScraper
    ( startPlayerListScraper
    ) where

import Control.Concurrent (forkIO, threadDelay, ThreadId)
import Control.Exception (try)
import Control.Monad (forever, void)
import Data.List (length)
import Data.Maybe (catMaybes)
import Network.HTTP.Simple (httpLBS, parseRequest, getResponseBody, HttpException)
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.ByteString.Lazy.Internal as BSLI
import qualified Database.Persist.Postgresql as P
import System.Random (randomRIO)
import Text.Regex.TDFA ((=~))

import Env
    ( Env
    , getPool
    , incrementScrapeCount
    )
import Models
    ( defaultPlayerList
    , defaultTag
    , getLastPlayerList
    , getOrInsertTag
    , insertPlayerList
    , PlayerList (..)
    , queueTag
    , Tag (..)
    )

-- URLS

overwatchTrackerPCURL :: String
overwatchTrackerPCURL =
    "https://overwatchtracker.com/leaderboards/pc/global/Level?mode=0&page="

overwatchTrackerPSNURL :: String
overwatchTrackerPSNURL =
    "https://overwatchtracker.com/leaderboards/psn/global/Level?mode=0&page="

overwatchTrackerXboxURL :: String
overwatchTrackerXboxURL =
    "https://overwatchtracker.com/leaderboards/xbox/global/Level?mode=0&page="

-- Regex

overwatchTrackerPCRegex :: BSLI.ByteString
overwatchTrackerPCRegex =
    "<a[^<>]*href=\"/profile/pc/(eu|kr|us)/[^\"]*\">([^#]+)#([0-9]+)</a>"

overwatchTrackerPSNRegex :: BSLI.ByteString
overwatchTrackerPSNRegex =
    "<a[^<>]*href=\"/profile/(psn)/global/[^\"]*\">([^<]+)</a>"

overwatchTrackerXboxRegex :: BSLI.ByteString
overwatchTrackerXboxRegex =
    "<a[^<>]*href=\"/profile/(xbox)/global/[^\"]*\">([^<]+)</a>"

-- Period to wait between scrapes, in microseconds.
scrapePeriod :: Int
-- scrapePeriod = 86400000000 -- 24hr
scrapePeriod = 7200000000 -- 2hr

-- Platforms
data Platform = PC | PSN | Xbox deriving Show

-- Get a random platform
randomPlatform :: IO Platform
randomPlatform = do
    index <- randomRIO (1, 3) :: IO Int
    case index of
        1 -> return PC
        2 -> return PSN
        3 -> return Xbox

-- Get the URL for a platform
getPlatformURL :: Platform -> String
getPlatformURL PC = overwatchTrackerPCURL
getPlatformURL PSN = overwatchTrackerPSNURL
getPlatformURL Xbox = overwatchTrackerXboxURL

-- Get the regex for a platform
getPlatformRegex :: Platform -> BSLI.ByteString
getPlatformRegex PC = overwatchTrackerPCRegex
getPlatformRegex PSN = overwatchTrackerPSNRegex
getPlatformRegex Xbox = overwatchTrackerXboxRegex

-- Start the scraper
startPlayerListScraper :: Env -> IO ThreadId
startPlayerListScraper env =
    forkIO (forever (do
        void (try (doScrape env) :: IO (Either HttpException ()))
        threadDelay scrapePeriod))

-- Scrape one tag from the queue.
doScrape :: Env -> IO ()
doScrape env = do
    -- Prelude.putStrLn "Scraping a player list"
    -- Get the Platform to scrape
    platform <- randomPlatform
    let url = getPlatformURL platform
    let regex = getPlatformRegex platform
    -- Get the last successful scrape for this url
    playerList <- getLastPlayerList (getPool env) url
    case playerList of
        Just playerList -> do
            Prelude.putStrLn ((show platform) ++ " has at least " ++
                (show (playerListPage playerList)) ++ " pages")
            scrapePlayerList env url regex (playerListPage playerList)
        Nothing ->
            scrapePlayerList env url regex 5000

-- Scrape a playerlist from a given url and page with given regex
scrapePlayerList :: Env -> String -> BSLI.ByteString -> Int -> IO ()
scrapePlayerList env url regex page = do
    -- Scrape up to 1.1x the highest successful scraped page
    -- This means 91% of the time it will scrape a page that is known to have
    -- players on it but 9% of the time it will try to scrape a new page
    scrapePage <- randomRIO (1, floor ((fromIntegral page) * 1.1)) :: IO Int
    let scrapeURL = url ++ (show scrapePage)
    -- Get the page contents
    request <- parseRequest scrapeURL
    response <- httpLBS request
    let body = getResponseBody response
    -- Get the matches
    let matches = (body =~ regex) :: [[BSLI.ByteString]]
    -- Convert into tags
    let tags = catMaybes (map matchToTag matches)
    -- Store PlayerList record
    let playerList = defaultPlayerList { playerListUrl = scrapeURL
                                       , playerListPage = scrapePage
                                       , playerListPlayercount = length tags
                                       }
    Prelude.putStrLn (show playerList)
    insertPlayerList (getPool env) playerList
    -- Queue all the tags
    sequence_ (map (queueScrapedTag env) tags)

-- Convert a regex match to a tag
matchToTag :: [BSLI.ByteString] -> Maybe Tag
matchToTag [match, region, battletag, code] =
    Just (defaultTag { tagServer = BSL8.unpack (BSL8.append "pc-" region)
                     , tagBattletag = BSL8.unpack battletag
                     , tagCode = BSL8.unpack code
                     })
matchToTag [match, "psn", battletag] =
    Just (defaultTag { tagServer = "psn"
                     , tagBattletag = BSL8.unpack battletag
                     })
matchToTag [match, "xbox", battletag] =
    Just (defaultTag { tagServer = "xbl"
                     , tagBattletag = BSL8.unpack battletag
                     })
matchToTag _ = Nothing

-- Add a tag skeleton to the DB and the queue
queueScrapedTag :: Env -> Tag -> IO ()
queueScrapedTag env tag = do
    maybeTagEntity <- getOrInsertTag (getPool env) tag
    case maybeTagEntity of
        Just (P.Entity {P.entityKey = tagKey, P.entityVal = tag}) -> do
            queueTag (getPool env) tagKey 3
            incrementScrapeCount env
        Nothing -> return ()
