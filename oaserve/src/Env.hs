module Env
    ( Env
    , decrementScrapeCount
    , getPool
    , getScrapeCount
    , incrementScrapeCount
    , makeEnv
    , setScrapeCount
    , startScraper
    , stopScraper
    , waitScraper
    ) where

import Control.Concurrent
    ( MVar
    , isEmptyMVar
    , newEmptyMVar
    , newMVar
    , putMVar
    , readMVar
    , takeMVar
    )
import qualified Database.Persist.Postgresql as P

data Env = Env
    { envPool :: P.ConnectionPool
    , envScrapeCount :: MVar Int
    , envScraping :: MVar ()
    }

-- Make an Env record.
makeEnv :: P.ConnectionPool -> IO Env
makeEnv pool = do
    -- Make scrape count MVar
    scrapeCount <- newMVar 0
    -- Make scraping MVar
    scraping <- newEmptyMVar
    -- Return Env
    return Env { envPool = pool
               , envScrapeCount = scrapeCount
               , envScraping = scraping
               }

-- Get the connection pool.
getPool :: Env -> P.ConnectionPool
getPool env =
    envPool env

-- Get the number of tags in the scrape queue
getScrapeCount :: Env -> IO Int
getScrapeCount env =
    readMVar (envScrapeCount env)

-- Set the number of tags in the scrape queue
setScrapeCount :: Env -> Int -> IO ()
setScrapeCount env newScrapeCount =
    takeMVar (envScrapeCount env) >> putMVar (envScrapeCount env) newScrapeCount

-- Increment the number of tags in the scrape queue.
incrementScrapeCount :: Env -> IO ()
incrementScrapeCount env = do
    takeMVar (envScrapeCount env) >>= putMVar (envScrapeCount env) . (1 +)
    startScraper env

-- Decrement the number of tags in the scrape queue.
decrementScrapeCount :: Env -> IO ()
decrementScrapeCount env =
    takeMVar (envScrapeCount env) >>= putMVar (envScrapeCount env) . (-1 +)

-- Wait until scraper should run
waitScraper :: Env -> IO ()
waitScraper env =
    readMVar (envScraping env)

-- Start the scraper
startScraper :: Env -> IO ()
startScraper env = do
    notScraping <- isEmptyMVar (envScraping env)
    if notScraping then
        putMVar (envScraping env) ()
    else
        return ()

-- Stop the scraper
stopScraper :: Env -> IO ()
stopScraper env = do
    notScraping <- isEmptyMVar (envScraping env)
    if notScraping then
        return ()
    else
        takeMVar (envScraping env)
