module Websockets
    ( broadcast
    , State
    , websocketApp
    ) where

import Control.Concurrent
    ( modifyMVar
    , MVar
    , newEmptyMVar
    , putMVar
    , takeMVar
    )
import Control.Exception (catch, SomeException)
import Control.Monad (foldM)
import Data.Text (pack, Text)
import Network.WebSockets
    ( acceptRequest
    , Connection
    , sendClose
    , sendTextData
    , ServerApp
    )

-- Types

type Client = (Connection, MVar Terminate)
type State = [Client]
type Terminate = ()

-- Connection request handler.
websocketApp :: MVar State -> ServerApp
websocketApp state pendingConnection = do
    putStrLn "hi :)"
    terminate <- newEmptyMVar
    connection <- acceptRequest pendingConnection
    connectClient connection terminate state
    takeMVar terminate
    sendClose connection (Data.Text.pack "bye (:")

-- Add a client connection.
connectClient :: Connection -> MVar Terminate -> MVar State -> IO ()
connectClient connection terminate state =
    modifyMVar state $ \oldState ->
        return ((connection, terminate):oldState, ())

-- Broadcast a message to all clients.
broadcast :: MVar State -> Text -> IO ()
broadcast state text =
    modifyMVar state $ \oldState -> do
        newState <- foldM (sendOrRemove text) [] oldState
        return (newState, ())

-- Send a message to a client and remove it if it fails.
sendOrRemove :: Text -> [Client] -> Client -> IO [Client]
sendOrRemove text clients client =
    catch (send text clients client) (handler clients client)
    where
        send :: Text -> [Client] -> Client -> IO [Client]
        send text clients client = do
            sendTextData (fst client) text
            return (client:clients)
        handler :: [Client] -> Client -> SomeException -> IO [Client]
        handler clients client exception = do
            putMVar (snd client) ()
            return clients
