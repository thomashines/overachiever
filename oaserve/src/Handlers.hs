{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Handlers
    ( getAchievementsHandler
    , getTagCountHandler
    , getTagHandler
    , indexHandler
    , notFoundHandler
    , playerHandler
    , queueTagHandler
    , staticFileHandler
    ) where

import Data.Aeson (encode)
import Data.Text (pack)
import Network.HTTP.Types (status200, status404)
import Network.HTTP.Types.URI (urlDecode)
import Network.Mime (defaultMimeLookup)
import Network.Wai (Application, responseLBS, responseFile)
import qualified Data.Aeson.TH as TH
import qualified Data.ByteString.Char8 as BS8
import qualified Database.Persist.Postgresql as P

import Env (Env, getPool, incrementScrapeCount)
import Models
    ( Achievement (..)
    , buildTag
    , getAchievements
    , getOrInsertTag
    , getTagCount
    , getTagUnlocksAchievements
    , queueTag
    , getTagById
    , Tag (..)
    , Unlock (..)
    )

-- API models

data AchievementAPI = AchievementAPI
    { achievement :: Achievement
    , unlocked :: Bool
    }

data TagAPI = TagAPI
    { tagId :: P.Key Tag
    , tag :: Tag
    , achievements :: [AchievementAPI]
    }

TH.deriveJSON TH.defaultOptions ''AchievementAPI
TH.deriveJSON TH.defaultOptions ''TagAPI

-- API methods

-- Convert an (unlock, achievement) pair into the achievement API form.
makeAchievementAPIFromPair :: (P.Entity Unlock, P.Entity Achievement)
    -> AchievementAPI
makeAchievementAPIFromPair (unlock, achievement) =
    AchievementAPI { achievement = P.entityVal achievement
                   , unlocked = unlockUnlocked (P.entityVal unlock)
                   }

-- Convert an achievement pair into the achievement API form.
makeAchievementAPIFromAchievement :: P.Entity Achievement -> AchievementAPI
makeAchievementAPIFromAchievement achievement =
   AchievementAPI { achievement = P.entityVal achievement
                  , unlocked = False
                  }

-- Handlers

-- 404 - File not found.
notFoundHandler :: Application
notFoundHandler request respond =
    respond $ responseLBS
        status404
        [("Content-Type", "application/json")]
        "[404]"

-- Serve a file, this will probably be redundant for a proper static HTTP server.
serveFile :: BS8.ByteString -> Application
serveFile filepath request respond =
    let filepathString = "../view/" ++ (BS8.unpack filepath)
    in respond $ responseFile
        status200
        [("Content-Type", defaultMimeLookup (Data.Text.pack filepathString))]
        filepathString
        Nothing

-- Serve a static file.
staticFileHandler :: [BS8.ByteString] -> Env -> Application
staticFileHandler vars env =
    case vars of
        (requestPath:[filepath]) ->
            serveFile filepath
        _ ->
            notFoundHandler

-- Serve the index.
indexHandler :: [BS8.ByteString] -> Env -> Application
indexHandler vars env =
    serveFile "Index.html"

-- Serve the player page.
playerHandler :: [BS8.ByteString] -> Env -> Application
playerHandler vars env =
    serveFile "Player.html"

-- Get a list of the achievements in the database.
getAchievementsHandler :: [BS8.ByteString] -> Env -> Application
getAchievementsHandler vars env request respond = do
    achievements <- getAchievements (getPool env)
    let achievementsAPI = map makeAchievementAPIFromAchievement achievements
    respond $ responseLBS
        status200
        [("Content-Type", "application/json")]
        (encode achievementsAPI)

-- Return the number of scraped tags in the database.
getTagCountHandler :: [BS8.ByteString] -> Env -> Application
getTagCountHandler vars env request respond = do
    count <- getTagCount (getPool env)
    respond $ responseLBS
        status200
        [("Content-Type", "application/json")]
        (encode [count])

-- Get the details and achievements for a tag from the database.
getTagHandler :: [BS8.ByteString] -> Env -> Application
getTagHandler vars env request respond = do
    -- case tail vars of
    --     [server, battletag, code] ->
    --         BS8.putStrLn (urlDecode False battletag)
    --     _ ->
    --         Prelude.putStrLn "bad req"
    case buildTag (tail vars) of
        Just skeletonTag -> do
            maybeTagEntity <- getOrInsertTag (getPool env) skeletonTag
            case maybeTagEntity of
                Just (P.Entity {P.entityKey = tagKey, P.entityVal = tag}) -> do
                    unlocksAchievements <- getTagUnlocksAchievements
                                            (getPool env)
                                            tagKey
                    let achievements = map
                                        makeAchievementAPIFromPair
                                        unlocksAchievements
                    respond $ responseLBS
                        status200
                        [("Content-Type", "application/json")]
                        (encode TagAPI { tagId = tagKey
                                       , tag = tag
                                       , achievements = achievements
                                       })
                Nothing ->
                    notFoundHandler request respond
        Nothing ->
            notFoundHandler request respond

-- Add a tag to the scraping queue.
queueTagHandler :: [BS8.ByteString] -> Env -> Application
queueTagHandler vars env request respond =
    case buildTag (tail vars) of
        Just skeletonTag -> do
            maybeTagEntity <- getOrInsertTag (getPool env) skeletonTag
            case maybeTagEntity of
                Just (P.Entity {P.entityKey = tagKey, P.entityVal = tag}) -> do
                    queueTag (getPool env) tagKey 1
                    incrementScrapeCount env
                    respond $ responseLBS
                        status200
                        [("Content-Type", "application/json")]
                        (encode TagAPI { tagId = tagKey
                                       , tag = tag
                                       , achievements = []
                                       })
                Nothing ->
                    notFoundHandler request respond
        Nothing ->
            notFoundHandler request respond
