# overachieve.me
Website is no longer hosted. Feel free to host an instance.

## Other Overwatch data sources:

* [masteroverwatch.com](https://masteroverwatch.com/)
    * [SneakySandy#1439](https://masteroverwatch.com/profile/pc/us/SneakySandy-1439)
    * [Leaderboards](https://masteroverwatch.com/leaderboards/)
        * Player list: https://masteroverwatch.com/leaderboards/pc/global/mode/ranked/category/skillrating/hero/overall/role/overall/data?offset=0
        * Shows <500 players
* [overbuff.com](https://www.overbuff.com/)
    * [SneakySandy#1439](https://www.overbuff.com/players/pc/SneakySandy-1439)
    * [Leaderboards](https://www.overbuff.com/rankings)
        * Shows 100 players per category, doesn't expose an API to get more
* [overwatchtracker.com](https://overwatchtracker.com/)
    * [SneakySandy#1439](https://overwatchtracker.com/profile/pc/us/SneakySandy-1439)
    * [Leaderboards](https://overwatchtracker.com/leaderboards/pc/global)
        * ~100 players per page, ~1800 pages, ~180,000 players
        * No API, only in HTML
        * [pc](https://overwatchtracker.com/leaderboards/pc/global/Level?mode=0&page=4142)
        * [psn](https://overwatchtracker.com/leaderboards/psn/global/Level?mode=0&page=1994)
        * [xbox](https://overwatchtracker.com/leaderboards/xbox/global/Level?mode=0&page=1620)
* [playoverwatch.com](https://playoverwatch.com/)
    * [SneakySandy#1439](https://playoverwatch.com/en-us/career/pc/us/SneakySandy-1439)
    * No player list
