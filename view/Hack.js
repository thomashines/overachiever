// Set the title
document.title = "overachieve";
var split = window.location.pathname.split("/");
if (split.length == 3) {
    battletag = split[2].split('-');
    document.title += " - " + battletag[0];
    if (battletag.length == 2) {
        document.title += "#" + battletag[1];
    }
    document.title += " [" + split[1] + "]";
}

// Clear the no JavaScript message.
document.addEventListener("DOMContentLoaded", function() {
    nojs = document.getElementById("nojs");
    nojs.parentElement.removeChild(nojs);
});
