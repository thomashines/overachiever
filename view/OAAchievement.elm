module OAAchievement exposing
    ( OAAchievement
    , default
    , compare
    , decoder
    , listDecoder
    , listRender
    , render
    )


import Json.Decode
import Basics

import Html exposing
    ( Html
    , div
    , p
    , img
    , text
    )
import Html.Attributes exposing
    ( class
    , src
    , style
    )

import UTF8 exposing
    ( toMultiByte
    )

-- Overachiever achievement struct.
type alias OAAchievement =
    { category : Int
    , description : String
    , id : Int
    , image : String
    , name : String
    , percent : Float
    , unlocked : Bool
    }


-- Blank achievement.
default : OAAchievement
default =
    { category = 0
    , description = ""
    , id = 0
    , image = ""
    , name = ""
    , percent = 0
    , unlocked = False
    }


-- Compare achievements.
compare : OAAchievement -> OAAchievement -> Order
compare left right =
    case (Basics.compare right.percent left.percent) of
        EQ ->
            Basics.compare right.name left.name
        x ->
            x


-- Decode a json string into an achievement.
decoder : Json.Decode.Decoder OAAchievement
decoder =
    Json.Decode.map7 OAAchievement
        (Json.Decode.at ["achievement", "achievementCategory"] Json.Decode.int)
        (Json.Decode.at ["achievement", "achievementDescription"] Json.Decode.string)
        (Json.Decode.at ["achievement", "achievementPoid"] Json.Decode.int)
        (Json.Decode.at ["achievement", "achievementImage"] Json.Decode.string)
        (Json.Decode.at ["achievement", "achievementName"] Json.Decode.string)
        (Json.Decode.at ["achievement", "achievementPercent"] Json.Decode.float)
        (Json.Decode.field "unlocked" Json.Decode.bool)


-- Decode a json string into a list of achievements.
listDecoder : Json.Decode.Decoder (List OAAchievement)
listDecoder =
    Json.Decode.list decoder


categoryName : Int -> String
categoryName cat =
    case cat of
        0 ->
            "Persistent"
        1 ->
            "Special"
        _ ->
            "unnamed"

-- Render the list of achievements
listRender : Bool -> (List OAAchievement) -> Html msg
listRender showUnlock achievements =
    let categories = List.range 0 1
        byCategory = List.map
                        (\cat -> (cat
                               , List.filter
                                    (\achieve -> achieve.category == cat)
                                    achievements))
                        categories
    in div
        [class "achievements"]
        (List.concatMap
            (\(cat, achieves) ->
                (if showUnlock then
                    let totalCount = List.length achieves
                        unlocked = List.foldl
                                    (\a b -> if a.unlocked then b + 1 else b)
                                    0
                                    achieves
                    in [p
                        [class "category"]
                        [text ((categoryName cat) ++
                               " achievements (unlocked " ++
                               (toString unlocked) ++ " of " ++
                               (toString totalCount) ++ "):")]]
                else
                    [p
                        [class "category"]
                        [text ((categoryName cat) ++ " achievements:")]])
                ++ (List.map
                    (render showUnlock)
                    (List.sortWith compare achieves)))
            byCategory)

-- Render an achievement to a table row.
render : Bool -> OAAchievement -> Html msg
render showUnlock achievement =
    div [class "achievement"] [
        div
            [class "achImg"]
            [img [src achievement.image] []],
        if showUnlock
            then div
                [if achievement.unlocked
                    then class "achRightImg"
                    else class "achRightImg disabled"
                ]
                [img [src achievement.image] []]
            else text "",
        div [class "achTextHolder"] [
            div [
                if achievement.percent <= 26 then
                    class "achBar legendary"
                else if achievement.percent <= 76 then
                    class "achBar epic"
                else if achievement.percent <= 317 then
                    class "achBar rare"
                else
                    class "achBar common",
                style [("width", (toString (achievement.percent / 10)) ++ "%")]
            ] [],
            div
                [class "achPercent"]
                [text ((toString (achievement.percent / 10)) ++ "% of players")],
            div [class "achText"] [
                p [class "achName"] [text (toMultiByte achievement.name)],
                p [class "achDescription"] [text (toMultiByte achievement.description)]
            ]
        ],
        div [class "clear"] []
    ]
