module NullTime exposing
    ( NullTime
    , default
    , decoder
    , toString
    )


import Date
import Json.Decode


-- Nullable time struct.
type alias NullTime =
    { time: String
    , valid: Bool
    }


-- Default NullTime.
default : NullTime
default =
    { time = ""
    , valid = False
    }


-- Decode a json string into a tag.
decoder : Json.Decode.Decoder NullTime
decoder =
    Json.Decode.map2 NullTime
        (Json.Decode.oneOf
            [ Json.Decode.string
            , Json.Decode.null "" ])
        (Json.Decode.oneOf
            [ Json.Decode.null False
            , Json.Decode.succeed True ])

-- Get string format of NullTime.
toString : NullTime -> String
toString nulltime =
    if nulltime.valid
        then nulltime.time
        else "never"
