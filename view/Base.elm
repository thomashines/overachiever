module Base exposing
    -- ( baseURL
    ( main
    , setLocation
    , init
    , Model
    , Msg(..)
    , update
    , makeOption
    , viewHeader
    , viewFooter
    )


import Array
import Json.Decode
import List
import Maybe
import String


import Http
import Navigation
import WebSocket


import OAAchievement
import OATag


import Html exposing
    ( Html
    , a
    , button
    , div
    , form
    , h1
    , i
    , input
    , option
    , p
    , select
    , text
    )
import Html.Attributes exposing
    ( action
    , class
    , disabled
    , href
    , id
    , method
    , name
    , placeholder
    , selected
    , type_
    , value
    )
import Html.Events exposing
    ( onClick
    , onInput)

import UTF8 exposing
    ( toMultiByte
    )


-- MAIN


main =
    Navigation.program setLocation {
        init = init,
        subscriptions = (\_ -> Sub.none),
        update = update,
        view = view
    }


-- Do not do anything with the location (actually handled in init).
setLocation : Navigation.Location -> Msg
setLocation location =
    DoNothing


-- MODEL


type alias Model =
    OATag.OATag


-- INIT


-- Return the skeleton tag.
init : Navigation.Location -> (OATag.OATag, Cmd Msg)
init _ =
    (OATag.default, Cmd.none)


-- UPDATE


-- Page events
type Msg
    = DoNothing
    | BattleTagChanged String
    | ServerChanged String


-- Page event handler
update : Msg -> OATag.OATag -> (OATag.OATag, Cmd Msg)
update msg oatag =
    case msg of
        DoNothing ->
            (oatag, Cmd.none)

        BattleTagChanged battletag ->
            (OATag.setBattleTag oatag battletag, Cmd.none)

        ServerChanged newServer ->
            ({oatag |
                server = newServer
            }, Cmd.none)


-- VIEW


view : OATag.OATag -> Html Msg
view oatag =
    div [class "body"] [
        viewHeader oatag,
        viewBody oatag,
        viewFooter oatag
    ]


viewHeader : OATag.OATag -> Html Msg
viewHeader oatag =
    div [class "header"] [
        div [class "title"] [a [href "/"] [text "overachieve"]],
        div [class "tagform"] [
            form [action (OATag.getURL oatag), method "POST"] [
                select
                    [onInput ServerChanged, id "server", name "server"]
                    (List.map
                        (makeOption oatag.server)
                        [ ("pc-us", "PC - United States")
                        , ("pc-eu", "PC - Europe")
                        , ("pc-kr", "PC - Korea")
                        , ("psn", "PS4")
                        , ("xbl", "Xbox One")
                        ]),
                input [ placeholder "BattleTag"
                      , onInput BattleTagChanged
                      , value (toMultiByte (OATag.getBattleTag oatag))
                      , id "username"
                      , name "username"
                      ] [],
                button [disabled (not (OATag.isValid oatag))] [text "Go"]
            ]
        ]
    ]

viewBody : OATag.OATag -> Html Msg
viewBody oatag =
    div [class "main"] [text "Base"]


viewFooter : OATag.OATag -> Html Msg
viewFooter oatag =
    div
        [class "footer"]
        [ text "Data only as up to date as that on "
        , a [href "https://playoverwatch.com/en-us/"] [text "playoverwatch.com"]
        ]


makeOption : String -> (String, String) -> Html Msg
makeOption defaultValue (optionValue, optionText) =
    if defaultValue == optionValue
        then option [value optionValue, selected True] [text optionText]
        else option [value optionValue] [text optionText]
