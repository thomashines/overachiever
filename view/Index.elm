module Index exposing (main)


import Html exposing
    ( Html
    , button
    , div
    , p
    , text
    )
import Html.Attributes exposing
    ( class
    )
import Html.Events exposing
    ( onClick
    )
import Navigation
import Http
import Json.Decode


import Base
import OATag
import OAAchievement


-- MAIN


main =
    Navigation.program setLocation {
        init = init,
        subscriptions = (\_ -> Sub.none),
        update = update,
        view = view
    }


-- Do not do anything with the location (actually handled in init).
setLocation : Navigation.Location -> Msg
setLocation location =
    BaseMsg Base.DoNothing


-- MODEL


type alias Model =
    { baseModel : Base.Model
    , scrapedCount : Int
    , achievements : List OAAchievement.OAAchievement
    , status : String
    }


-- INIT


-- Return the skeleton tag.
init : Navigation.Location -> (Model, Cmd Msg)
init location =
    ({ baseModel = OATag.default
     , scrapedCount = 0
     , achievements = []
     , status = "getting achievements"
     }, getOAAchievements)


-- UPDATE


-- Page events
type Msg
    = BaseMsg Base.Msg
    | GotOAAchievements (Result Http.Error (List OAAchievement.OAAchievement))
    | GotScrapedCount (Result Http.Error Int)


-- Transform Base.update result
baseUpdate : Model -> (Base.Model, Cmd Base.Msg) -> (Model, Cmd Msg)
baseUpdate model result =
    ({model |
        baseModel = Tuple.first result
     }, Cmd.map (\m -> BaseMsg m) (Tuple.second result))


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        BaseMsg baseMsg ->
            baseUpdate
                model
                (Base.update baseMsg model.baseModel)

        GotOAAchievements (Err err) ->
            ({model |
                status = (toString err)
             }, Cmd.none)

        GotOAAchievements (Ok newAchievements) ->
            ({model |
                achievements = newAchievements,
                status = ""
             }, getScrapedCount)

        GotScrapedCount (Err err) ->
            ({model |
                status = (toString err)
             }, Cmd.none)

        GotScrapedCount (Ok newScrapedCount) ->
            ({model |
                scrapedCount = newScrapedCount,
                status = ""
             }, Cmd.none)



-- VIEW


view : Model -> Html Msg
view model =
    div [class "body"] [
        Html.map (\m -> BaseMsg m) (Base.viewHeader model.baseModel),
        viewBody model,
        Html.map (\m -> BaseMsg m) (Base.viewFooter model.baseModel)
    ]


viewBody : Model -> Html Msg
viewBody model =
    div [class "main"] [
        if not (String.isEmpty model.status) then
            p [class "status"] [text ("Status: " ++ model.status)]
        else
            text "",
        p
            [class "status"]
            [text ((toString model.scrapedCount) ++ " players counted.")],
        if List.isEmpty model.achievements then
            p [class "status"] [text "No achievement data."]
        else
            OAAchievement.listRender False model.achievements
    ]


-- HTTP


getScrapedCount : Cmd Msg
getScrapedCount =
    Http.send
        GotScrapedCount
        (Http.get
            "/api/global/count"
            (Json.Decode.index 0 Json.Decode.int))


getOAAchievements : Cmd Msg
getOAAchievements =
    Http.send
        GotOAAchievements
        (Http.get
            "/api/global"
            OAAchievement.listDecoder)
