module OATag exposing
    ( OATag
    , default
    , build
    , getAPIURL
    , getBattleTag
    , getURL
    , isValid
    , decoder
    , setBattleTag
    , setBattletagAndCode
    )


import Json.Decode
import Http exposing
    ( decodeUri
    , encodeUri
    )

import UTF8 exposing
    ( toMultiByte
    )


import NullTime
import OAAchievement


-- Overachiever tag struct.
type alias OATag =
    { achievements: List OAAchievement.OAAchievement
    , battletag : String
    , code : String
    , failed : NullTime.NullTime
    , id : Int
    , scraped : NullTime.NullTime
    , server : String
    }


-- Default tag.
default : OATag
default =
    { achievements = []
    , battletag = ""
    , code = ""
    , failed = NullTime.default
    , id = 0
    , scraped = NullTime.default
    , server = "pc-us"
    }


-- Build a tag with a list containing [server, battletag].
build : List String -> OATag
build details =
    setBattletagAndCode
        {default |
            server = Maybe.withDefault "" (List.head details)
        }
        (String.split
            "-"
            (Maybe.withDefault
                ""
                (decodeUri
                    (Maybe.withDefault
                        ""
                        (List.head (List.drop 1 details))
                    )
                )
            )
        )


-- Get the battletag for a tag (e.g. "SneakySandy#1439").
getBattleTag : OATag -> String
getBattleTag oatag =
    if String.isEmpty oatag.code
        then oatag.battletag
        else oatag.battletag ++ "#" ++ oatag.code


-- Get the page URL for a tag.
getURL : OATag -> String
getURL oatag =
    if String.isEmpty oatag.code
        then "/" ++ oatag.server ++ "/" ++ (encodeUri (toMultiByte oatag.battletag))
        else "/" ++ oatag.server ++ "/" ++ (encodeUri (toMultiByte oatag.battletag)) ++ "-" ++ oatag.code


-- Get the API URL for a tag.
getAPIURL : OATag -> String
getAPIURL oatag =
    "/api" ++ (getURL oatag)


-- Does the tag have all required properties?
isValid : OATag -> Bool
isValid oatag =
    not (String.isEmpty oatag.server || String.isEmpty oatag.battletag ||
        ((String.startsWith "pc" oatag.server) && String.isEmpty oatag.code))


-- Decode a json string into a tag.
decoder : Json.Decode.Decoder OATag
decoder =
    Json.Decode.map7 OATag
        (Json.Decode.oneOf
            [ Json.Decode.field "achievements" OAAchievement.listDecoder
            , Json.Decode.field "achievements" (Json.Decode.null [])
            ]
        )
        (Json.Decode.at ["tag", "tagBattletag"] Json.Decode.string)
        (Json.Decode.at ["tag", "tagCode"] Json.Decode.string)
        (Json.Decode.at ["tag", "tagFailed"] NullTime.decoder)
        (Json.Decode.field "tagId" Json.Decode.int)
        (Json.Decode.at ["tag", "tagScraped"] NullTime.decoder)
        (Json.Decode.at ["tag", "tagServer"] Json.Decode.string)


-- Update a tag with a battletag (e.g. "SneakySandy#1439").
setBattleTag : OATag -> String -> OATag
setBattleTag oatag battletag =
    if String.right 1 battletag == "#"
        then setBattletagAndCode oatag [battletag, ""]
        else setBattletagAndCode oatag (String.split "#" battletag)


-- Update a tag with a list containing [battletag, code]
setBattletagAndCode : OATag -> List String -> OATag
setBattletagAndCode oatag battletagAndCode =
    {oatag |
        battletag =
            Maybe.withDefault "" (List.head (List.drop 0 battletagAndCode)),
        code =
            Maybe.withDefault "" (List.head (List.drop 1 battletagAndCode))
    }
