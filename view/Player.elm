module Player exposing (main)

import Debug exposing
    ( log
    )

import Html exposing
    ( Html
    , button
    , div
    , i
    , p
    , text
    )
import Html.Attributes exposing
    ( class
    )
import Html.Events exposing
    ( onClick
    )
import Http
import Navigation
import WebSocket

import UTF8 exposing
    ( toMultiByte
    )


import NullTime
import OAAchievement
import OATag
import Base


-- CONSTANTS


scrapedURL : String
scrapedURL =
    -- "ws://0.0.0.0:8000/api/scraped"
    "wss://overachieve.me/api/scraped"


-- MAIN


main =
    Navigation.program setLocation {
        init = parseLocation,
        subscriptions = subscriptions,
        update = update,
        view = view
    }


-- Do not do anything with the location (actually handled in init).
setLocation : Navigation.Location -> Msg
setLocation location =
    BaseMsg Base.DoNothing


-- MODEL


type alias Model =
    { baseModel : Base.Model
    , player : OATag.OATag
    , status : String
    }


-- INIT


-- Return the skeleton tag and request the tag data from the API.
init : OATag.OATag -> (Model, Cmd Msg)
init oatag =
    ({ baseModel = oatag
     , player = oatag
     , status = "init status"
     }, getOATag oatag)


-- Split the URL path on forward slash, make tag with components.
parseLocation : Navigation.Location -> (Model, Cmd Msg)
parseLocation location =
    init (OATag.build (List.drop 1 (String.split "/" location.pathname)))


-- UPDATE


-- Page events
type Msg
    = BaseMsg Base.Msg
    | GetOATag
    | GotMessage String
    | GotOATag (Result Http.Error OATag.OATag)
    | RefreshedOATag (Result Http.Error OATag.OATag)
    | RefreshOATag


-- Transform Base.update result
baseUpdate : Model -> (Base.Model, Cmd Base.Msg) -> (Model, Cmd Msg)
baseUpdate model result =
    ({model |
        baseModel = Tuple.first result
     }, Cmd.map (\m -> BaseMsg m) (Tuple.second result))


-- Page event handler
update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        BaseMsg baseMsg ->
            baseUpdate
                model
                (Base.update baseMsg model.baseModel)

        GetOATag ->
            ({model |
                status = "getting data..."
             },
             getOATag model.player)

        GotMessage message ->
            (model,
             if (log "ws" message) == (toString model.player.id)
                 then getOATag model.player
                 else Cmd.none)

        GotOATag (Err err) ->
            ({model |
                status = (toString err)
             }, Cmd.none)

        GotOATag (Ok newoatag) ->
            ({model |
                player = newoatag,
                status = ""
             }, Cmd.none)

        RefreshedOATag (Err err) ->
            ({model |
                status = (toString err)
             }, Cmd.none)

        RefreshedOATag (Ok _) ->
            ({model |
                status = "refreshing..."
             }, Cmd.none)

        RefreshOATag ->
            (model, refreshOATag model.player)


-- VIEW


view : Model -> Html Msg
view model =
    div [class "body"] [
        Html.map (\m -> BaseMsg m) (Base.viewHeader model.baseModel),
        viewBody model,
        Html.map (\m -> BaseMsg m) (Base.viewFooter model.baseModel)
    ]


viewBody : Model -> Html Msg
viewBody model =
    div [class "main"] [
        div [class "tagtitle"] [
            p [class ("server " ++ model.player.server)] [text model.player.server],
            p [class "battletag"] [text (toMultiByte (OATag.getBattleTag model.player))],
            button [class "refresh", onClick RefreshOATag] []
        ],
        if String.isEmpty model.status then
            text ""
        else
            p [class "status"] [text ("Status: " ++ model.status)],
        p [class "status"] [text ("Refreshed: " ++ (NullTime.toString model.player.scraped))],
        if model.player.failed.valid && ((not model.player.scraped.valid) ||
                (model.player.failed.time > model.player.scraped.time)) then
            p [class "status"] [text ("Failed to refresh: " ++ (NullTime.toString model.player.failed))]
        else
            text "",
        if List.isEmpty model.player.achievements then
            p [class "status"] [text "No achievement data."]
        else
            OAAchievement.listRender True model.player.achievements
    ]


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    WebSocket.listen scrapedURL GotMessage


-- HTTP


getOATag : OATag.OATag -> Cmd Msg
getOATag oatag =
    Http.send
        GotOATag
        (Http.get
            (OATag.getAPIURL oatag)
            OATag.decoder)


refreshOATag : OATag.OATag -> Cmd Msg
refreshOATag oatag =
    Http.send
        RefreshedOATag
        (Http.post
            (OATag.getAPIURL oatag)
            Http.emptyBody
            OATag.decoder)
