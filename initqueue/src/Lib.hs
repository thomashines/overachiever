module Lib
    ( enqueueFile
    ) where

import Control.Monad
import Data.List.Split
import Network.HTTP.Simple
import System.Environment

enqueueFile :: IO ()
enqueueFile = do
    args <- getArgs
    case args of
        [] ->
            putStrLn "./Main players.txt"
        [filename] -> do
            fileString <- readFile filename
            forM_ (lines fileString) enqueue

baseURL :: String
baseURL =
    -- "http://0.0.0.0:8000/api/"
    "http://overachieve.me/api/"

enqueue :: String -> IO ()
enqueue string =
    let url = baseURL ++ string
    in do
        request <- parseRequest ("POST " ++ url)
        _ <- httpNoBody request
        putStrLn url
